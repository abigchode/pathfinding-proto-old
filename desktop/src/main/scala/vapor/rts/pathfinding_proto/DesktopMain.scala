package vapor.rts.pathfinding_proto

import com.badlogic.gdx.backends.lwjgl._

object DesktopMain extends App {
    val cfg = new LwjglApplicationConfiguration
    cfg.title = "pathfinding-proto"
    cfg.height = 480
    cfg.width = 800
    cfg.forceExit = false
    new LwjglApplication(Main, cfg)
}
