package vapor.rts.pathfinding_proto

import com.badlogic.gdx.backends.android._

class DesktopMain extends AndroidApplication {
  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)
    val config = new AndroidApplicationConfiguration
    config.useAccelerometer = false
    config.useCompass = false
    config.useWakelock = true
    config.hideStatusBar = true
    initialize(Main, config)
  }
}
