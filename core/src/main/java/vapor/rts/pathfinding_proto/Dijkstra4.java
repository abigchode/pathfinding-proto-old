package vapor.rts.pathfinding_proto;

import java.util.*;

public class Dijkstra4 {
    public interface EndReached<T> {
        public boolean endReached(T node);
    }

    public interface GetMoveCost<T> {
        public float getMoveCost(T a, T b);
    }

    public interface GetSurroundingNodes<T> {
        public ArrayList<T> getSurroundingNodes(T t);
    }

    private static class Node<T> {
        private T node;
        private Optional<Node<T>> parent;
        private float cost;

        public Node(T node, Optional<Node<T>> parent, float cost) {
            this.node = node;
            this.parent = parent;
            this.cost = cost;
        }
    }

    public static <T> ArrayList<T> search(T start, EndReached endReached, GetMoveCost<T> getMoveCost, GetSurroundingNodes<T> getSurroundingNodes) {
        HashMap<T, Node<T>> searchedNodes = new HashMap<>();
        PriorityQueue<Node<T>> frontier = new PriorityQueue<>(100, (a, b) -> Float.compare(a.cost, b.cost));

        Node<T> startNode = new Node(start, Optional.empty(), 0);
        searchedNodes.put(start, startNode);
        frontier.add(startNode);

        while(frontier.size() > 0) {
            Node<T> current = frontier.remove();

            if(endReached.endReached(current.node)) {
                ArrayList<T> path = new ArrayList<>();
                Optional<Node<T>> pathCurrent = Optional.of(current);
                while (pathCurrent.isPresent()) {
                    path.add(0, pathCurrent.get().node);
                    pathCurrent = pathCurrent.get().parent;
                }
                return path;
            }

            ArrayList<T> surrounding = getSurroundingNodes.getSurroundingNodes(current.node);
            for(T next : surrounding) {
                Node<T> searchedNode = searchedNodes.get(next);
                float newCost = current.cost + getMoveCost.getMoveCost(current.node, next);

                if (searchedNode == null) {
                    Node<T> newNode = new Node<T>(next, Optional.of(current), newCost);
                    searchedNodes.put(next, newNode);
                    frontier.add(newNode);
                }
                else if (newCost < searchedNode.cost) {
                    searchedNode.parent = Optional.of(current);
                    searchedNode.cost = newCost;
                }
            }
        }

        return null;
    }

    public static ArrayList<Tile> search(Tile start, EndReached endReached) {
        return Dijkstra4.search(
            start,
            endReached,
            (a, b) -> a.getMoveCost(b),
            Tile:: getSurroundingNodes
        );
    }
}
