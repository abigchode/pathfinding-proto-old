package vapor.rts.pathfinding_proto;

import java.util.ArrayList;

public class Tile implements Dijkstra.Node<Tile> {
    private static float sqrt2Over2 = (float)Math.sqrt(2) / 2;

    private Map _map;

    public int x;
    public int y;
    public boolean passable;

    public Tile(Map map, int x, int y) {
        this._map = map;
        this.x = x;
        this.y = y;
        this.passable = true;
    }

    @Override
    public ArrayList<Tile> getSurroundingNodes() {
        ArrayList<Tile> tiles = new ArrayList<>();
        for(int i = -1; i <= 1; i++) {
            for(int j = -1; j <= 1; j++) {
                Tile tile = _map.getTile(x + i, y + j);
                if(tile != null && tile.passable) {
                    tiles.add(tile);
                }
            }
        }
        return tiles;
    }

    @Override
    public float getMoveCost(Tile other) {
        if (this.x - other.x == 0 || this.y - other.y == 0) return Tile.sqrt2Over2;
        else return 1;
    }

    @Override
    public String toString() {
        return "x: " + x + " y: " + y;
    }
}
