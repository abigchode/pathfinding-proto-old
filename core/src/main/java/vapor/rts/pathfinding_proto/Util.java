package vapor.rts.pathfinding_proto;

public class Util {
    public static float distance2(float x1, float y1, float x2, float y2) {
        float xDiff = x1 - x2;
        float yDiff = y1 - y2;
        return xDiff * xDiff + yDiff * yDiff;
    }

    public static float distance(float x1, float y1, float x2, float y2) {
        return (float)Math.sqrt(distance2(x1, y1, x2, y2));
    }
}
