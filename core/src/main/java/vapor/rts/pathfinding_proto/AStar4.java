package vapor.rts.pathfinding_proto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.PriorityQueue;

public class AStar4 {
    public interface GetMoveCost<T> {
        public float getMoveCost(T a, T b);
    }

    public interface GetHeuristic<T> {
        public float getHeuristic(T a, T b);
    }

    public interface GetSurroundingNodes<T> {
        public ArrayList<T> getSurroundingNodes(T t);
    }

    private static class Node<T> {
        public T node;
        public Optional<Node<T>> parent;
        public float g;
        public float h;

        public float getCost() {
            return g + h;
        }

        public Node(T node, Optional<Node<T>> parent, float g, float h) {
            this.node = node;
            this.parent = parent;
            this.g = g;
            this.h = h;
        }
    }

    public static <T> ArrayList<T> search(T start, T end, GetMoveCost<T> getMoveCost, GetHeuristic<T> getHeuristic, GetSurroundingNodes<T> getSurroundingNodes) {
        HashMap<T, Node<T>> searchedNodes = new HashMap<>();
        PriorityQueue<Node<T>> frontier = new PriorityQueue<>(
            100,
            (a, b) -> Float.compare(a.getCost(), b.getCost())
        );

        Node<T> startNode = new Node<T>(start, Optional.empty(), 0, getHeuristic.getHeuristic(start, end));
        searchedNodes.put(start, startNode);
        frontier.add(startNode);

        while (frontier.size() > 0) {
            Node<T> current = frontier.remove();

            if (current.node == end) {
                ArrayList<T> path = new ArrayList<>();
                Optional<Node<T>> pathCurrent = Optional.of(current);
                while (pathCurrent.isPresent()) {
                    path.add(0, pathCurrent.get().node);
                    pathCurrent = pathCurrent.get().parent;
                }
                return path;
            }

            ArrayList<T> surrounding = getSurroundingNodes.getSurroundingNodes(current.node);
            for(T next : surrounding) {
                Node<T> searchedNode = searchedNodes.get(next);
                float newCost = current.g + getMoveCost.getMoveCost(current.node, next);

                if (searchedNode == null) {
                    Node<T> newNode = new Node<T>(next, Optional.of(current), newCost, getHeuristic.getHeuristic(next, end));
                    searchedNodes.put(next, newNode);
                    frontier.add(newNode);
                }
                else if (newCost < searchedNode.g) {
                    searchedNode.parent = Optional.of(current);
                    searchedNode.g = newCost;
                }
            }
        }

        return null;
    }

    public static ArrayList<Tile> search(Tile start, Tile end) {
        return AStar4.search(
                start,
                end,
                (a, b) -> a.getMoveCost(b),
                (a, b) -> Math.abs(a.x - b.x) + Math.abs(a.y - b.y),
                Tile::getSurroundingNodes
        );
    }
}
