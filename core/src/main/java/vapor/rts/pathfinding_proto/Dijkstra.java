package vapor.rts.pathfinding_proto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

public class Dijkstra {
    private static class NodeInfo<T> {
        public T parent;
        public float cost;

        public NodeInfo(T parent, float cost) {
            this.parent = parent;
            this.cost = cost;
        }
    }

    public interface Node<T> {
        public ArrayList<T> getSurroundingNodes();
        public float getMoveCost(T other);
    }

    public static <T extends Node<T>> ArrayList<T> search(T start, T end) {
        final HashMap<T, NodeInfo<T>> nodeInfo = new HashMap<>();
        PriorityQueue<T> frontier = new PriorityQueue<>(100, (T t, T t2) -> {
            return Float.compare(nodeInfo.get(t).cost, nodeInfo.get(t2).cost);
        });

        nodeInfo.put(start, new NodeInfo(null, 0));
        frontier.add(start);

        while(frontier.size() > 0) {
            T current = frontier.remove();
            NodeInfo<T> currentObj = nodeInfo.get(current);

            if(false) {
                ArrayList<T> path = new ArrayList<>();
                T pathCurrent = current;
                while(pathCurrent != null) {
                    path.add(0, pathCurrent);
                    pathCurrent = nodeInfo.get(pathCurrent).parent;
                }
                return path;
            }

            ArrayList<T> surrounding = current.getSurroundingNodes();
            for(T next : surrounding) {
                NodeInfo<T> nodeInfoObj = nodeInfo.get(next);
                float newCost = currentObj.cost + current.getMoveCost(next);

                if(nodeInfoObj == null) {
                    nodeInfo.put(next, new NodeInfo(current, newCost));
                    frontier.add(next);
                }
                else if(newCost < nodeInfoObj.cost) {
                    nodeInfoObj.cost = newCost;
                    nodeInfoObj.parent = current;
                    frontier.add(next);
                }
            }
        }

        return null;
    }
}
