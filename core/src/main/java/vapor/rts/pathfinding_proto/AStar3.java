package vapor.rts.pathfinding_proto;

import java.util.*;

public class AStar3 {
    public interface GetMoveCost<T> {
        public float getMoveCost(T a, T b);
    }

    public interface GetHeuristic<T> {
        public float getHeuristic(T a, T b);
    }

    public interface GetSurroundingNodes<T> {
        public ArrayList<T> getSurroundingNodes(T t);
    }

    private static class ANode<T> implements Comparable<ANode<T>> {
        public T node;
        public Optional<ANode<T>> parent;
        public float g;
        public float h;

        public float getCost() {
            return g + h;
        }

        public ANode(T node, Optional<ANode<T>> parent, float g, float h) {
            this.node = node;
            this.parent = parent;
            this.g = g;
            this.h = h;
        }

        @Override
        public int compareTo(ANode<T> that) {
            return Float.compare(this.g + this.h, that.g + that.h);
        }
    }

    public static <T> ArrayList<T> search(T start, T end, GetMoveCost<T> getMoveCost, GetHeuristic<T> getHeuristic, GetSurroundingNodes<T> getSurroundingNodes) {
        HashSet<T> searched = new HashSet<>();
        HashMap<T, ANode<T>> toSearchHash = new HashMap<>();
        PriorityQueue<ANode<T>> toSearch = new PriorityQueue<>();

        toSearch.add(new ANode(start, Optional.empty(), 0, getHeuristic.getHeuristic(start, end)));

        while(toSearch.size() > 0) {
            ANode<T> current = toSearch.remove();
            toSearchHash.remove(current.node);
            searched.add(current.node);

            if(current.node == end) {
                ArrayList<T> path = new ArrayList<>();
                Optional<ANode<T>> pathCurrent = Optional.of(current);
                while(pathCurrent.isPresent()) {
                    path.add(0, pathCurrent.get().node);
                    pathCurrent = pathCurrent.get().parent;
                }
                return path;
            }

            ArrayList<T> newNodes = getSurroundingNodes.getSurroundingNodes(current.node);
            for(T newNode : newNodes) {
                if(!searched.contains(newNode)) {
                    ANode<T> searchedNode = toSearchHash.get(newNode);
                    if(searchedNode == null) {
                        float g = getMoveCost.getMoveCost(current.node, newNode) + current.g;
                        float h = getHeuristic.getHeuristic(newNode, end);
                        ANode<T> add = new ANode(newNode, Optional.of(current), g, h);
                        toSearch.add(add);
                        toSearchHash.put(newNode, add);
                    }
                    else {
                        float newG = getMoveCost.getMoveCost(current.node, newNode) + current.g;
                        if(newG < searchedNode.g) {
                            searchedNode.g = newG;
                            searchedNode.parent = Optional.of(current);
                        }
                    }
                }
            }
        }

        return null;
    }

    public static ArrayList<Tile> search(Tile start, Tile end) {
        return AStar3.search(
                start,
                end,
                (a, b) -> a.getMoveCost(b),
                (a, b) -> Math.abs(a.x - b.x) + Math.abs(a.y - b.y),
                Tile::getSurroundingNodes
        );
    }
}
