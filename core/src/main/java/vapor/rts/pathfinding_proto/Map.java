package vapor.rts.pathfinding_proto;

public class Map {
    private int _width;
    private int _height;
    private Tile[][] _tiles;

    public Map(int width, int height) {
        _width = width;
        _height = height;
        _tiles = new Tile[width][height];

        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                Tile tile = new Tile(this, i, j);
                _tiles[i][j] = tile;
            }
        }
    }

    public Tile getTile(int x, int y) {
        if(x >= 0 && y >= 0 && x < _width && y < _height) {
            return _tiles[x][y];
        }
        return null;
    }
}
