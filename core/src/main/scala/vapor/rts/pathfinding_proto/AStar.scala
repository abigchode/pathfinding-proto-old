package vapor.rts.pathfinding_proto

import scala.collection.mutable

object AStar {
    trait Node[T] {
        def node: T
        def parent: Option[Node[T]]

        def path(): List[T] = recursivePath().reverse

        private def recursivePath(): List[T] = parent match {
            case Some(n) => node :: n.recursivePath()
            case None => List(node)
        }
    }

    def search[T](start: T, end: T, getMoveCost: (T, T) => Float, getHeuristic: (T, T) => Float, getSurroundingNodes: T => Seq[T]): Seq[T] = {
        class ANode(val node: T, var parent: Option[ANode], var g: Float, val h: Float) extends Node[T] with Ordered[ANode] {
            def cost = g + h

            override def compare(that: ANode) = {
                if(this.cost < that.cost) -1
                else if(this.cost > that.cost) 1
                else 0
            }
        }

        val searched = new java.util.HashSet[T]()
        val toSearchHash = new java.util.HashMap[T, ANode]()
        val toSearch = new java.util.PriorityQueue[ANode]()

        toSearch.add(new ANode(start, None, 0, getHeuristic(start, end)))

        while(toSearch.size() > 0) {
            val current = toSearch.remove()
            toSearchHash.remove(current.node)
            searched.add(current.node)

            if(current.node == end) {
                //return current.path()
                val path = new mutable.ArrayBuffer[T]()
                var pathCurrent = Option(current)
                while (pathCurrent.nonEmpty) {
                    path += pathCurrent.get.node
                    pathCurrent = pathCurrent.get.parent
                }
                return path.reverse
            }

            val newNodes = getSurroundingNodes(current.node)
            var i = 0
            while(i < newNodes.length) {
                val newNode = newNodes(i)
                if(!searched.contains(newNode)) {
                    val searchedNode = toSearchHash.get(newNode)
                    if(searchedNode == null) {
                        val g = getMoveCost(current.node, newNode) + current.g
                        val h = getHeuristic(newNode, end)
                        val add = new ANode(newNode, Option(current), g, h)
                        toSearch.add(add)
                        toSearchHash.put(newNode, add)
                    }
                    else {
                        val newG = getMoveCost(current.node, newNode) + current.g
                        if(newG < searchedNode.g) {
                            searchedNode.g = newG
                            searchedNode.parent = Option(current)
                        }
                    }
                }
                i += 1
            }
        }

        Nil
    }
}
