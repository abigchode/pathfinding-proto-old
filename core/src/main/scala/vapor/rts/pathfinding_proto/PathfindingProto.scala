package vapor.rts.pathfinding_proto

import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.FPSLogger
import scala.collection.JavaConverters._
import scala.collection.mutable.ArrayBuffer

class PathfindingProto extends Screen {
    private val _fpsLogger = new FPSLogger()
    private val _map = new Map(256, 256)

    override def render(delta: Float): Unit = {
        /*Time {
            var i = 0
            while(i < 100) {
                val path = AStar2.search[Tile](
                    _map.getTile(0, 255),
                    _map.getTile(255, 0),
                    (a, b) => a.getMoveCost(b),
                    (a, b) => Math.abs(a.x - b.x) + Math.abs(a.y - b.y),
                    t => {
                        val tiles = new ArrayBuffer[Tile]()
                        var i = -1
                        while(i <= 1) {
                            var j = -1
                            while(j <= 1) {
                                val tile = _map.getTile(t.x + i, t.y + j)
                                if(tile != null && tile.passable) {
                                    tiles += tile
                                }
                                j += 1
                            }
                            i += 1
                        }
                        tiles
                    }
                )
                i += 1
            }
        }*/
        _fpsLogger.log()
    }

    override def hide(): Unit = {

    }

    override def resize(width: Int, height: Int): Unit = {

    }

    override def dispose(): Unit = {

    }

    override def pause(): Unit = {

    }

    override def show(): Unit = {

    }

    override def resume(): Unit = {

    }
}
