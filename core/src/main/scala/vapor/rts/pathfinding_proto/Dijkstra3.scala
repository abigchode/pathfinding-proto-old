package vapor.rts.pathfinding_proto

import java.util.Comparator

import scala.collection.mutable

object Dijkstra3 {
    def search[T](start: T, endReached: T => Boolean, getMoveCost: (T, T) => Float, getSurroundingNodes: T => Seq[T]): Seq[T] = {
        case class Node(node: T, var parent: Option[Node], var cost: Float)

        val searchedNodes = new java.util.HashMap[T, Node]()
        val frontier = new java.util.PriorityQueue[Node](100, new Comparator[Node] {
            override def compare(x: Node, y: Node): Int = {
                if(x.cost < y.cost) -1
                else if(x.cost > y.cost) 1
                else 0
            }
        })

        val startNode = Node(start, None, 0)
        searchedNodes.put(start, startNode)
        frontier.add(startNode)

        while (frontier.size() > 0) {
            val current = frontier.remove()

            if (endReached(current.node)) {
                val path = new mutable.ArrayBuffer[T]()
                var pathCurrent = Option(current)
                while (pathCurrent.nonEmpty) {
                    path += pathCurrent.get.node
                    pathCurrent = pathCurrent.get.parent
                }
                return path.reverse
            }

            val surrounding = getSurroundingNodes(current.node)
            var i = 0
            while(i < surrounding.length) {
                val next = surrounding(i)
                val searchedNode = searchedNodes.get(next)
                lazy val newCost = current.cost + getMoveCost(current.node, next)

                if (searchedNode == null) {
                    val newNode = Node(next, Some(current), newCost)
                    searchedNodes.put(next, newNode)
                    frontier.add(newNode)
                }
                else if (newCost < searchedNode.cost) {
                    searchedNode.parent = Some(current)
                    searchedNode.cost = newCost
                }
                i += 1
            }
        }

        Nil
    }
}
