package vapor.rts.pathfinding_proto

import java.util.Comparator

import scala.collection.mutable

object AStar5 {
    def search[T](start: T, endReached: T => Boolean, getMoveCost: (T, T) => Float, getHeuristic: T => Float, getSurroundingNodes: T => Seq[T]): Seq[T] = {
        case class Node(node: T, var parent: Option[Node], var g: Float, h: Float) {
            def cost = g + h
        }

        val searchedNodes = new java.util.HashMap[T, Node]()
        val frontier = new java.util.PriorityQueue[Node](100, new Comparator[Node] {
            override def compare(x: Node, y: Node): Int =
            //x.cost.compareTo(y.cost)
            {
                if(x.cost < y.cost) -1
                else if(x.cost > y.cost) 1
                else 0
            }
        })

        val startNode = Node(start, None, 0, getHeuristic(start))
        searchedNodes.put(start, startNode)
        frontier.add(startNode)

        while (frontier.size() > 0) {
            val current = frontier.remove()

            if (endReached(current.node)) {
                val path = new mutable.ArrayBuffer[T]()
                var pathCurrent = Option(current)
                while (pathCurrent.nonEmpty) {
                    path += pathCurrent.get.node
                    pathCurrent = pathCurrent.get.parent
                }
                return path.reverse
            }

            val surrounding = getSurroundingNodes(current.node)
            var i = 0
            while(i < surrounding.length) {
                val next = surrounding(i)
                val searchedNode = searchedNodes.get(next)
                lazy val newCost = current.g + getMoveCost(current.node, next)

                if (searchedNode == null) {
                    val newNode = Node(next, Some(current), newCost, getHeuristic(next))
                    searchedNodes.put(next, newNode)
                    frontier.add(newNode)
                }
                else if (newCost < searchedNode.g) {
                    searchedNode.parent = Some(current)
                    searchedNode.g = newCost
                }
                i += 1
            }
        }

        Nil
    }

    def searchEnd[T](start: T, end: T, getMoveCost: (T, T) => Float, getHeuristic: T => Float, getSurroundingNodes: T => Seq[T]): Seq[T] = {
        AStar5.search[T](start, (t: T) => t == end, getMoveCost, getHeuristic, getSurroundingNodes)
    }

    def searchNoH[T](start: T, endReached: T => Boolean, getMoveCost: (T, T) => Float, getSurroundingNodes: T => Seq[T]): Seq[T] = {
        AStar5.search[T](start, endReached, getMoveCost, (t: T) => 0, getSurroundingNodes)
    }
}
