package vapor.rts.pathfinding_proto

import scala.collection.mutable

object WavefrontExpansion {
    def apply[T](start: T, endNodeReached: T => Boolean, getSurrounding: T => Seq[T], endIterationReached: Int => Boolean = i => false) {
        val openList = new mutable.Queue[T]()
        openList += start
        val closedList = new mutable.HashSet[T]()
        closedList += start

        var iteration = 0
        while(openList.length > 0) {
            val iterationSize = openList.length
            var i = 0
            while(i < iterationSize) {
                val currentNode = openList.dequeue()
                if(endNodeReached(currentNode)) {
                    return
                }
                getSurrounding(currentNode).filter(n => !closedList.contains(n)).foreach(n => {
                    openList += n
                    closedList += n
                })
                i += 1
            }
            iteration += 1
            if(endIterationReached(iteration)) {
                return
            }
        }
    }
}
