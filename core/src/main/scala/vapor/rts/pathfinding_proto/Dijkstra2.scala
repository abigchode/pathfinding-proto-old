package vapor.rts.pathfinding_proto

import java.util.Comparator

import scala.collection.mutable

object Dijkstra2 {
    def search[T <: AnyRef](start: T, endReached: T => Boolean, getMoveCost: (T, T) => Float, getSurroundingNodes: T => Seq[T]): Seq[T] = {
        case class NodeInfo(var parent: Option[T], var cost: Float)

        val nodeInfos = new java.util.HashMap[T, NodeInfo]()
        val frontier = new java.util.PriorityQueue[T](100, new Comparator[T] {
            override def compare(x: T, y: T): Int = nodeInfos.get(x).cost compare nodeInfos.get(y).cost
        })

        nodeInfos.put(start, NodeInfo(None, 0))
        frontier.add(start)

        while(frontier.size() > 0) {
            val current = frontier.remove()
            val currentObj = nodeInfos.get(current)

            if(endReached(current)) {
                val path = new mutable.ArrayBuffer[T]()
                var pathCurrent = Option(current)
                while(pathCurrent.nonEmpty) {
                    path += pathCurrent.get
                    pathCurrent = nodeInfos.get(pathCurrent.get).parent
                }
                return path
            }

            getSurroundingNodes(current).foreach { next =>
                val nodeInfoObj = nodeInfos.get(next)
                lazy val newCost = currentObj.cost + getMoveCost(current, next)

                if(nodeInfoObj == null) {
                    nodeInfos.put(next, NodeInfo(Some(current), newCost))
                    frontier.add(next)
                }
                else if(newCost < nodeInfoObj.cost) {
                    nodeInfoObj.parent = Some(current)
                    nodeInfoObj.cost = newCost
                }
            }
        }

        Nil
    }
}
