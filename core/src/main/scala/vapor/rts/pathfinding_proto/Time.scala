package vapor.rts.pathfinding_proto

object Time {
    def apply[T](block: => T) = {
        val start = System.currentTimeMillis()
        val result = block
        val time = System.currentTimeMillis() - start
        println(s"time: $time")
        result
    }
}
