package vapor.rts.pathfinding_proto

import com.badlogic.gdx.Game
import vapor.rts.pathfinding_proto.Dijkstra4.EndReached
import scala.collection.JavaConverters._
import scala.collection.mutable.ArrayBuffer

object Main extends Game {
    private var _game: PathfindingProto = null

    override def create(): Unit = {
        val _map = new Map(256, 256)

        def getSurroundingScala(t: Tile) = {
            val tiles = new ArrayBuffer[Tile]()
            var i = -1
            while(i <= 1) {
                var j = -1
                while(j <= 1) {
                    val tile = _map.getTile(t.x + i, t.y + j)
                    if(tile != null && tile.passable) {
                        tiles += tile
                    }
                    j += 1
                }
                i += 1
            }
            tiles
        }

        for(i <- 0 until 254) {
            _map.getTile(i, i).passable = false
            _map.getTile(i + 1, i).passable = false
        }

        Time {
            var i = 0
            while(i < 1) {
                WavefrontExpansion[Tile](_map.getTile(0, 255), _ => false, getSurroundingScala)
                i += 1
            }
        }

        /*Time {
            var i = 0
            while(i < 100) {
                val path = Dijkstra.search[Tile](
                    _map.getTile(0, 0),
                    _map.getTile(0, 0)
                )
                i += 1
            }
        }
        Time {
            var i = 0
            while(i < 100) {
                val path = Dijkstra2.search[Tile](
                    _map.getTile(0, 0),
                    _ => false,
                    (a, b) => a.getMoveCost(b),
                    _.getSurroundingNodes.asScala
                )
                i += 1
            }
        }*/
        Time {
            var i = 0
            while(i < 1) {
                val path = Dijkstra3.search[Tile](
                    _map.getTile(0, 255),
                    //_ == _map.getTile(255, 0),
                    _ => false,
                    (a, b) => a.getMoveCost(b),
                    getSurroundingScala
                )
                i += 1
            }
        }

        /*Time {
            var i = 0
            while(i < 100) {
                val path = Dijkstra4.search(
                    _map.getTile(0, 255),
                    new EndReached[Tile] {
                        override def endReached(node: Tile): Boolean = false//node == _map.getTile(255, 0)
                    }
                )
                i += 1
            }
        }*/

        /*Time {
            var i = 0
            while(i < 100) {
                val path = AStar3.search(
                    _map.getTile(0, 255),
                    _map.getTile(255, 0)
                )
                i += 1
            }
        }

        Time {
            var i = 0
            while(i < 100) {
                val path = AStar4.search(
                    _map.getTile(0, 255),
                    _map.getTile(255, 0)
                )
                i += 1
            }
        }*/

        /*Time {
            var i = 0
            while(i < 100) {
                val path = AStar.search[Tile](
                    _map.getTile(0, 255),
                    _map.getTile(255, 0),
                    (a, b) => a.getMoveCost(b),
                    (a, b) => Math.abs(a.x - b.x) + Math.abs(a.y - b.y),
                    getSurroundingScala

                )
                i += 1
            }
        }*/

        Time {
            val start = _map.getTile(0, 255)
            val end = _map.getTile(255, 0)
            var i = 0
            while(i < 100) {
                val path = AStar2.search[Tile](
                    start,
                    end,
                    //null,
                    (a, b) => a.getMoveCost(b),
                    (a, b) => Math.abs(a.x - b.x) + Math.abs(a.y - b.y),
                    //(a, b) => 0,
                    getSurroundingScala
                )
                i += 1
            }
        }

        Time {
            val start = _map.getTile(0, 255)
            val end = _map.getTile(255, 0)
            var i = 0
            while(i < 100) {
                val path = AStar5.search[Tile](
                    start,
                    _ == end,
                    //_ => false,
                    (a, b) => a.getMoveCost(b),
                    a => Math.abs(a.x - end.x) + Math.abs(a.y - end.y),
                    //a => 0,
                    getSurroundingScala
                )
                i += 1
            }
        }

        /*val path = AStar.search[Tile](
            _map.getTile(0, 255),
            _map.getTile(255, 0),
            (a, b) => a.getMoveCost(b),
            (a, b) => Math.abs(a.x - b.x) + Math.abs(a.y - b.y),
            //(a, b) => Util.distance(a.x, a.y, b.x, b.y),
            _.getSurroundingNodes.asScala
        )

        val path2 = AStar2.search[Tile](
            _map.getTile(0, 255),
            _map.getTile(255, 0),
            (a, b) => a.getMoveCost(b),
            (a, b) => Math.abs(a.x - b.x) + Math.abs(a.y - b.y),
            _.getSurroundingNodes.asScala
        )

        val zipped = path.zip(path2)
        for((a, b) <- zipped) {
            println(s"a: $a, b: $b, a == b: ${a == b}")
        }
        println(s"path1 length: ${path.length}")
        println(s"path2 length: ${path2.length}")*/

        _game = new PathfindingProto()
        setScreen(_game)
    }
}
